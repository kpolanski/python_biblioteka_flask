CREATE TABLE `users` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(16) NOT NULL DEFAULT '0' COLLATE 'utf8_unicode_ci',
	`password` VARCHAR(32) NOT NULL DEFAULT '0' COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

CREATE TABLE `books` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`author` VARCHAR(64) NOT NULL DEFAULT '0' COLLATE 'utf8_unicode_ci',
	`name` VARCHAR(64) NOT NULL DEFAULT '0' COLLATE 'utf8_unicode_ci',
	`year` YEAR NOT NULL DEFAULT 0000,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB
;

INSERT INTO `users` VALUES (0, 'admin', '21232f297a57a5a743894a0e4a801fc3');