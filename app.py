from flask import Flask, flash, redirect, render_template, request, session, url_for
from storage import Storage
import os

app = Flask(__name__)
app.secret_key = os.urandom(12)

@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return redirect(url_for('list_books'))


@app.route('/login', methods=['POST'])
def do_admin_login():
    db = Storage()
    if db.user_credentials(request.form['username'], request.form['password']) == 1:
        session['logged_in'] = True
    else:
        flash('Błędne hasło lub użytkownik', "danger")
    return redirect(url_for('home'))


@app.route("/logout")
def logout():
    session['logged_in'] = False
    return redirect(url_for('home'))


@app.route("/list")
def list_books():
    if 'logged_in' in session and session['logged_in'] == False:
        return redirect(url_for('home'))
    db = Storage()
    return render_template('books.html', books=db.get_all_books())


@app.route("/delete/<int:book_id>")
def delete_book(book_id):
    if 'logged_in' in session and session['logged_in'] == False:
        return redirect(url_for('home'))
    db = Storage()
    db.delete_book(book_id)
    flash("Książka usunięta", "success")
    return redirect(url_for('list_books'))


@app.route('/book/add', methods=["GET"])
def add_book_form():
    if 'logged_in' in session and session['logged_in'] == False:
        return redirect(url_for('home'))
    return render_template('add_book.html')


@app.route('/book/add', methods=["POST"])
def add_book():
    if 'logged_in' in session and session['logged_in'] == False:
        return redirect(url_for('home'))
    db = Storage()
    db.insert_new_book(book_author=request.form['book_author'],
                       book_name=request.form['book_name'],
                       book_year=request.form['book_year'])
    flash("Dodano nową książkę. Możesz dodawać dalej.", "success")
    return redirect(url_for('add_book_form'))


@app.route('/book/edit/<int:book_id>', methods=["GET"])
def edit_book_form(book_id):
    if 'logged_in' in session and session['logged_in'] == False:
        return redirect(url_for('home'))
    db = Storage()
    book = db.get_single_book(book_id=book_id)

    return render_template('edit_book.html', book=book)


@app.route('/book/edit', methods=["POST"])
def edit_book():
    if 'logged_in' in session and session['logged_in'] == False:
        return redirect(url_for('home'))
    db = Storage()
    db.update_single_book(book_author=request.form['book_author'],
                          book_name=request.form['book_name'],
                          book_year=request.form['book_year'],
                          book_id=request.form['book_id'])
    flash("Książka zmodyfikowana.", "success")
    return redirect(url_for('list_books'))


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=80)
