import mysql.connector


class Storage:
    def __init__(self):
        self.conn = mysql.connector.connect(
            host="localhost",
            user="ppp",
            passwd="ppp",
            database="ppp"
        )

    def user_credentials(self, username, password):
        cursor = self.conn.cursor()
        query = "SELECT COUNT(id) > 0 as value FROM users WHERE username = %s AND password = MD5(%s)"
        cursor.execute(query, (username, password))
        record = cursor.fetchone()
        valid = int(record[0])
        return valid

    def get_all_books(self):
        cursor = self.conn.cursor()
        query = "SELECT * FROM books"
        cursor.execute(query)
        records = cursor.fetchall()
        return records

    def delete_book(self, book_id):
        cursor = self.conn.cursor()
        query = "DELETE FROM books WHERE id = %s"
        result = cursor.execute(query, (book_id,))
        self.conn.commit()
        return result

    def insert_new_book(self, book_author, book_name, book_year):
        cursor = self.conn.cursor()
        query = "INSERT INTO books (author, name, year) VALUES (%s, %s, %s)"
        result = cursor.execute(query, (book_author, book_name, book_year))
        self.conn.commit()
        return result

    def get_single_book(self, book_id):
        cursor = self.conn.cursor()
        query = "SELECT * FROM books WHERE id = %s"
        cursor.execute(query, (book_id,))
        record = cursor.fetchone()
        return record

    def update_single_book(self, book_author, book_name, book_year, book_id):
        cursor = self.conn.cursor()
        query = "UPDATE books SET author = %s, name = %s, year = %s WHERE id = %s"
        result = cursor.execute(query, (book_author, book_name, book_year, book_id,))
        self.conn.commit()
        return result
